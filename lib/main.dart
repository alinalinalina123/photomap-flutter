import 'package:flutter/material.dart';
import 'signUp.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'FlutterPhotoMap',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new LoginPage(title: 'Login'),
    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {


  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Padding(padding: new EdgeInsets.only(left: 30.0, right: 30.0),
              child: new TextField(
              decoration: new InputDecoration(
                  labelText: 'Enter email'),
                  keyboardType: TextInputType.emailAddress,
              )
            ),

            new Padding(padding: new EdgeInsets.only(left: 30.0, right: 30.0),
                child: new TextFormField(
                    decoration: new InputDecoration(
                        labelText: 'Enter password'
                       ), obscureText: true,
                )
            ),

            new Padding(padding: new EdgeInsets.only(top: 30.0),
                child: new RaisedButton(onPressed: () {

                },
                  child: const Text("Login"),
                  color: Colors.blue,
                  splashColor: Colors.white,
                  elevation: 6.0,
                )
            ),

            new Padding(padding: new EdgeInsets.only(top: 30.0, left: 50.0, right: 50.0),
                child: new Row(
                  children: <Widget>[
                    new Text("Don't have an account?"),
                    new Padding(padding: new EdgeInsets.only(left: 20.0)),
                    new FlatButton(child: new Text("Sign up", style: new TextStyle(color: Colors.pink,fontSize: 18.0)),
                      onPressed: () {
                        Navigator.of(context).push(
                          new MaterialPageRoute(
                            builder: (context) {
                              return new SignUpPage(title: "SignUp");
                            },
                          ),
                        );
                      },)]
                )
            )
          ],
        ),
      )
    );
  }
}
