import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  SignUpPageState createState() => new SignUpPageState();
}

class SignUpPageState extends State<SignUpPage> {


  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Padding(padding: new EdgeInsets.only(left: 30.0, right: 30.0),
                  child: new TextField(
                    decoration: new InputDecoration(
                        labelText: 'Enter email'),
                    keyboardType: TextInputType.emailAddress,
                  )
              ),

              new Padding(padding: new EdgeInsets.only(left: 30.0, right: 30.0),
                  child: new TextFormField(
                    decoration: new InputDecoration(
                        labelText: 'Enter password'
                    ), obscureText: true,
                  )
              ),

              new Padding(padding: new EdgeInsets.only(top: 30.0),
                  child: new RaisedButton(onPressed: () {

                  },
                    child: const Text("Sign Up"),
                    color: Colors.blue,
                    splashColor: Colors.white,
                    elevation: 6.0,
                  )
              )
            ],
          ),
        )
    );
  }
}
